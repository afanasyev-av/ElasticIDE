#include "optionsdialog.h"
#include "ui_optionsdialog.h"

OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);
    ui->CompilerlineEdit->setText(compiler);
    highlighter = new Highlighter(ui->plainTextEdit->document());
    ui->plainTextEdit->appendPlainText("#include <>\n class a{ }\n SELECT\n");
}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

void OptionsDialog::on_pushButton_pressed()
{
    QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"","");
    ui->CompilerlineEdit->setText(FileName);
    compiler = FileName;
}

void OptionsDialog::on_pushButton_2_clicked()
{

}

void OptionsDialog::on_pushButton_2_pressed()
{
    QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"","");
    ui->ExecuterlineEdit->setText(FileName);
    executer = FileName;
}

void OptionsDialog::on_pushButton_3_clicked()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok,ui->pushButton->font(), this);
    if (ok) {
        //ui->pushButton->setFont(font);
        highlighter->keywordFormat.setFont(font);
    }
    //QColor color = QColorDialog::getColor(Qt::yellow, this);
}
