#include "mainwindow.h"
#include "ui_mainwindow.h"

//constructor
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/images/ico.png"));
    setWindowTitle("Elastic IDE");

    connect(&myProc,SIGNAL(readyReadStandardOutput()),this,SLOT(showOutput()));
    connect(&myProc,SIGNAL(readyReadStandardError()),this,SLOT(showOutput()));
    connect(ui->tabWidget, SIGNAL(tabCloseRequested ( int )), this, SLOT(closeTab(int)));

    ui->tabWidget->clear();
    ui->tabWidget->setTabsClosable(true);
    QList<int> list;
    list << 200 << 1;
    ui->splitter->setSizes(list);

    QSettings setting("elasticIDE.ini", QSettings::IniFormat);
    compiler = setting.value("compilers/compilers", "").toString();
}
//close tab
void MainWindow::closeTab(int index)
{
    ui->tabWidget->removeTab(index);
}
//destructor
MainWindow::~MainWindow()
{
    delete ui;
}
//open file menu
void MainWindow::on_actionOpen_triggered()
{
    QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("Elastic file (*.epl)"));
    if(FileName.isEmpty())return;
    openFile(FileName);
}
//open file
void MainWindow::openFile(QString FileName)
{
    QFile file(FileName);
    file.open(QFile::ReadOnly | QFile::Text);
    QTextStream ReadFile(&file);
    //ReadFile.setCodec(textcodec);
    EditTab *tab = new EditTab(this);
    connect(tab->getDocument(), SIGNAL(contentsChanged()),this, SLOT(documentWasModified()));
    ui->tabWidget->addTab(tab,QFileInfo(FileName).fileName());
    tab->FileName = FileName;
    tab->setText(ReadFile.readAll());
    file.close();
}
//if document modified
void MainWindow::documentWasModified()
{
    EditTab *tab;
    tab = (EditTab*)(ui->tabWidget->currentWidget());
    if(tab->getDocument()->isModified()){
        ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),QFileInfo(tab->FileName).fileName()+"[*]");
    }
    else{
        ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),QFileInfo(tab->FileName).fileName());
    }
}
//save as menu
void MainWindow::on_actionSaveAs_triggered()
{
    EditTab *tab;
    tab = (EditTab*)(ui->tabWidget->currentWidget());
    QString FileName = QFileDialog::getSaveFileName(this, tr("Save File"), tab->FileName,tr("Elastic file (*.epl)"));
    if (!FileName.isEmpty()) {
        if(!FileName.endsWith(".epl")){
            FileName.append(".epl");
        }

        QFile file(FileName);
        if (file.open(QIODevice::WriteOnly)) {
            QTextStream stream(&file);
            stream << tab->getText();
            ui->textEditDebug->append(tab->getText());
            file.flush();
            file.close();
            tab->FileName = FileName;
            tab->getDocument()->setModified(false);
            documentWasModified();
        }
        file.close();
    }
}
//run program
void MainWindow::on_actionRun_triggered()
{
    ui->textEditDebug->clear();
    EditTab *tab;
    tab = (EditTab*)(ui->tabWidget->currentWidget());
    QFile file(tab->FileName);
    if (file.open(QIODevice::WriteOnly)) {
        QTextStream stream(&file);
        stream << tab->getText();
        file.flush();
        file.close();
        tab->getDocument()->setModified(false);
        documentWasModified();
    }
    QString BinDir;
    BinDir = QFileInfo(tab->FileName).path() + "/Bin";
    if(QDir(BinDir).exists()){
        //Получаем список файлов
        QStringList lstFiles = QDir(BinDir).entryList(QDir::Files);
        //Удаляем файлы
        foreach (QString entry, lstFiles){
            QString entryAbsPath = QDir(BinDir).absolutePath() + "/" + entry;
            QFile::remove(entryAbsPath);
        }
        QDir().rmdir(BinDir);
        QDir().mkdir(BinDir);
    }

    if(compiler.isEmpty()){
        QMessageBox::warning(0,"Warning", "Select compiler");
        return;
    }

    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("LD_LIBRARY_PATH", "/home/alex82/platform");
    myProc.setProcessEnvironment(env);

    QString prog = compiler + " " + tab->FileName;
    myProc.start(prog);
}
//output program result
void MainWindow::showOutput()
{
    ui->textEditDebug->append(QString::fromLocal8Bit(myProc.readAllStandardOutput()));
    ui->textEditDebug->append(QString::fromLocal8Bit(myProc.readAllStandardError()));
}
//stop programming
void MainWindow::on_actionStop_triggered()
{
    myProc.terminate();
    ui->textEditDebug->clear();
}
//new file
void MainWindow::on_actionNew_triggered()
{
    EditTab *tab = new EditTab(this);
    ui->tabWidget->addTab(tab,"noname.epl");
    tab->FileName = "";
    tab->getDocument()->setModified(false);
    connect(tab->getDocument(), SIGNAL(contentsChanged()),this, SLOT(documentWasModified()));
}
//save file
void MainWindow::on_actionSave_triggered()
{
    EditTab *tab;
    tab = (EditTab*)(ui->tabWidget->currentWidget());
    if (!tab->FileName.isEmpty()) {
        QFile file(tab->FileName);
        if (file.open(QIODevice::WriteOnly)) {
            QTextStream stream(&file);
            stream << tab->getText();
            ui->textEditDebug->append(tab->getText());
            file.flush();
            file.close();
            tab->getDocument()->setModified(false);
            documentWasModified();
        }
        file.close();
    }
    else{
        emit on_actionSaveAs_triggered();
    }
}
//settings
void MainWindow::on_actionSettings_triggered()
{
    OptionsDialog dialog;
    //dialog.compiler = compiler;
    if (dialog.exec()){
        compiler = dialog.compiler;
        QSettings setting("elasticIDE.ini", QSettings::IniFormat);
        setting.setValue("compilers/compilers",compiler);
        executer = dialog.executer;
    }
}
//find
void MainWindow::on_actionFind_triggered()
{
    EditTab *tab;
    tab = (EditTab*)(ui->tabWidget->currentWidget());
    QTextDocument *document = tab->getDocument();

    QTextCursor Cursor(document);
    Cursor.beginEditBlock();

    bool ok;
    searchString = QInputDialog::getText(this, tr("Find "), tr("Find string:"), QLineEdit::Normal, "", &ok);
    if (!(ok && !searchString.isEmpty())){
        return;
    }
    cursor = QTextCursor(document);

    cursor = document->find(searchString, cursor, QTextDocument::FindWholeWords);

    if (!cursor.isNull()) {
        cursor.movePosition(QTextCursor::WordRight, QTextCursor::KeepAnchor);
        tab->getPlainText()->setTextCursor(cursor);
    }
    Cursor.endEditBlock();
}
//find next
void MainWindow::on_actionFind_next_triggered()
{
    EditTab *tab;
    tab = (EditTab*)(ui->tabWidget->currentWidget());
    QTextDocument *document = tab->getDocument();

    QTextCursor Cursor(document);
    Cursor.beginEditBlock();

    if(searchString.isEmpty()){
        bool ok;
        searchString = QInputDialog::getText(this, tr("Find "), tr("Find string:"), QLineEdit::Normal, "", &ok);
        if (!(ok && !searchString.isEmpty())){
            return;
        }
        cursor = QTextCursor(document);
    }

    cursor = document->find(searchString, cursor, QTextDocument::FindWholeWords);

    if (!cursor.isNull()) {
        cursor.movePosition(QTextCursor::WordRight, QTextCursor::KeepAnchor);
        tab->getPlainText()->setTextCursor(cursor);
    }
    Cursor.endEditBlock();
}
//compare 2 files
void MainWindow::on_actionCompare_triggered()
{
    EditTab *tab;
    tab = (EditTab*)(ui->tabWidget->currentWidget());

    QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("Elastic file (*.epl)"));
    if(FileName.isEmpty())return;
    QFile file(FileName);

    file.open(QFile::ReadOnly | QFile::Text);

    QTextStream ReadFile(&file);
    tab->setCompareText(ReadFile.readAll());
    tab->ShowCompare(true);
}
//codecs
void MainWindow::on_actionSet_Codec_triggered()
{
    textcodec = QTextCodec::codecForName("Windows-1251");
    emit on_actionSave_triggered();
    EditTab *tab;
    tab = (EditTab*)(ui->tabWidget->currentWidget());
    QFile file(tab->FileName);
    file.open(QFile::ReadOnly | QFile::Text);
    QTextStream ReadFile(&file);
    ReadFile.setCodec(textcodec);
    tab->setText(ReadFile.readAll());
}
//open include file
void MainWindow::openFileatCursor()
{
    EditTab *tab;
    tab = (EditTab*)(ui->tabWidget->currentWidget());
    QTextCursor cursor = tab->getPlainText()->textCursor();
    cursor.beginEditBlock();
    cursor.movePosition(QTextCursor::StartOfLine);
    cursor.movePosition(QTextCursor::EndOfLine, QTextCursor::KeepAnchor);
    cursor.endEditBlock();
    QString str = cursor.selectedText();
    int start = str.indexOf('"');
    int end = str.indexOf('"',start+1);
    QString FileName = str.mid(start+1,end-start-1);
    QString Dir;
    Dir = QFileInfo(tab->FileName).path();//home/user
    //QMessageBox::warning(0,"Warning", Dir);
    FileName.replace("$CUR_SCR_DIR/","");
    while(FileName.indexOf("../")==0){
        Dir = QFileInfo(Dir).path();
        //QMessageBox::warning(0,"Warning", Dir);
        //FileName.replace("../","");
        FileName.remove(0,3);
    }
    //QMessageBox::warning(0,"Warning", Dir);
    FileName = Dir+"/"+FileName;
    //QMessageBox::warning(0,"Warning", FileName);
    openFile(FileName);
}
