#-------------------------------------------------
#
# Project created by QtCreator 2016-08-08T20:29:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ElasticIDE
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    highlighter.cpp \
    codeeditor.cpp \
    edittab.cpp \
    optionsdialog.cpp

HEADERS  += mainwindow.h \
    highlighter.h \
    codeeditor.h \
    edittab.h \
    optionsdialog.h

FORMS    += mainwindow.ui \
    edittab.ui \
    optionsdialog.ui

DISTFILES +=

RESOURCES += \
    elasticide.qrc
