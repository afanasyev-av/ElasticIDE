#include "highlighter.h"

Highlighter::Highlighter(QTextDocument *parent)
     : QSyntaxHighlighter(parent)
{
    //ключевые слова
    keywordFormat.setForeground(Qt::darkBlue);
    keywordFormat.setFontWeight(QFont::Bold);
    m_lstKeywords << "include_file" << "for" << "print"
                     << "if" << "else" << "define"
                     <<"true" << "false"
                     << "return" << "ptr" << "count"
                     << "class" << "public" << "protected" << "typedef"
                     << "struct" << "new" << "while" << "operator" << "break"
                     << "elastic" << "asm" << "define_f" << "do" << "continue"
                     << "switch" << "case"
                     ;

     typeFormat.setForeground(Qt::darkMagenta);
     m_lstTypewords <<  "int" << "string" <<"float" << "color"
                  <<  "point" << "vector" <<"variant" << "int64"
                  <<  "char" << "BYTE" <<"SBYTE" << "WORD"
                  << "SWORD" << "DWORD" << "SDWORD" << "QWORD"
                  << "SQWORD" << "bm_float" << "m_double" << "void"
                     ;

     databaseFormat.setForeground(Qt::darkYellow);
     m_lstDBwords << "SELECT" << "FROM" << "TO" << "INSERT" << "WHERE"
                  << "UPDATE" << "INTO" <<"ORDER BY" << "VALUES"
                  << "OBJECT" << "WAIT_FOR_END" << "CID_OUT" << "OID_OUT"
                  << "SET OBJECT" << "CREATE" << "ARRAY" << "HSELECT"
                  << "SUB_COL" << "TO_ARRAY" << "GET ARRAY"
                       ;

     //комментарий
     CommentFormat.setForeground(Qt::darkGreen);

     //строки
     cstringFormat.setForeground(Qt::darkGreen);

     //препроцессор
     preprocessorFormat.setForeground(Qt::darkBlue);
}

void Highlighter::highlightBlock(const QString &str)
{
    int nState = previousBlockState();
    int nStart = 0;
    for (int i = 0; i < str .length(); ++i){
        if (nState == InsideCStyleComment) {
            if (str.mid(i, 2) == "*/") {
                nState = NormalState;
                setFormat(nStart,i - nStart + 2,CommentFormat);
                i++;
            }
        }
        else if (nState == InsideCString) {
            if (str.mid(i, 1) == "\"") {
                if (str .mid(i - 1, 2) != "\\\"") {
                    nState = NormalState;
                    setFormat(nStart,i - nStart + 1,cstringFormat);
                }
            }
        }
        else if (nState == InsidePHPString) {
            if (str.mid(i, 1) == "\'") {
                if (str.mid(i - 1, 2) != "\\\'") {
                    nState = NormalState;
                    setFormat(nStart,i - nStart + 1,cstringFormat);
                }
            }
        }
        else {
            if (str.mid(i, 2) == "//") {
                setFormat(i, str.length() - i, CommentFormat);
                break;
            }
            else if (str.mid(i, 1) == "#") {
                setFormat (i, str.length() - i, preprocessorFormat);
                break;
            }
            //else if (str .at (i) . isNumber()) {
            //    setFormat(i, 1, Qt::cyan);
            //}
            else if (str.mid(i, 2) == "/*") {
                nStart = i;
                nState = InsideCStyleComment;
            }
            else if (str.mid(i, 1) == "\""){
                nStart = i;
                nState = InsideCString;
            }
            else if (str.mid(i, 1) == "\'"){
                nStart = i;
                nState = InsidePHPString;
            }
            else {
                QString strKeyword = getKeyword(i, str);
                if (!strKeyword.isEmpty()) {
                    setFormat(i, strKeyword.length(), keywordFormat);
                    i += strKeyword.length() - 1;
                }
                QString strTypeword = getTypeword(i, str);
                if (!strTypeword.isEmpty()) {
                    setFormat(i, strTypeword.length(), typeFormat);
                    i += strTypeword.length() - 1;
                }
                QString strDBword = getDBword(i, str);
                if (!strDBword.isEmpty()) {
                    setFormat(i, strDBword.length(), databaseFormat);
                    i += strDBword.length() - 1;
                }
            }
        }
    }
    if (nState == InsideCStyleComment) {
        setFormat(nStart, str.length() - nStart, CommentFormat);
    }
    if (nState == InsideCString) {
        setFormat(nStart, str.length() - nStart, cstringFormat);
    }
    setCurrentBlockState(nState) ;
}

QString Highlighter::getKeyword(int nPos, const QString str)
{
    QString strTemp = "";
    bool separatorLeft;
    bool separatorRight;
    foreach(QString strKeyword, m_lstKeywords){
        separatorLeft = false;
        if(nPos == 0)separatorLeft = true;
        else if(!str[nPos-1].isLetterOrNumber())separatorLeft = true;
        separatorRight = false;
        if(nPos + strKeyword.length() > str.length()-1)separatorRight = true;
        else if(!str[nPos+strKeyword.length()].isLetterOrNumber())separatorRight = true;
        if(str.mid(nPos, strKeyword.length() ) == strKeyword && separatorLeft && separatorRight) {
            strTemp = strKeyword;
            break;
        }
    }
    return strTemp;
}

QString Highlighter::getTypeword(int nPos, const QString str)
{
    QString strTemp = "";
    bool separatorLeft;
    bool separatorRight;
    foreach(QString strTypeword, m_lstTypewords){
        separatorLeft = false;
        if(nPos == 0)separatorLeft = true;
        else if(!str[nPos-1].isLetterOrNumber())separatorLeft = true;
        separatorRight = false;
        if(nPos + strTypeword.length() > str.length()-1)separatorRight = true;
        else if(!str[nPos+strTypeword.length()].isLetterOrNumber())separatorRight = true;
        if(str.mid(nPos, strTypeword.length() ) == strTypeword && separatorLeft && separatorRight) {
            strTemp = strTypeword;
            break;
        }
    }
    return strTemp;
}

QString Highlighter::getDBword(int nPos, const QString str)
{
    QString strTemp = "";
    bool separatorLeft;
    bool separatorRight;
    foreach(QString strTypeword, m_lstDBwords){
        separatorLeft = false;
        if(nPos == 0)separatorLeft = true;
        else if(!str[nPos-1].isLetterOrNumber())separatorLeft = true;
        separatorRight = false;
        if(nPos + strTypeword.length() > str.length()-1)separatorRight = true;
        else if(!str[nPos+strTypeword.length()].isLetterOrNumber())separatorRight = true;
        if(str.mid(nPos, strTypeword.length() ) == strTypeword && separatorLeft && separatorRight) {
            strTemp = strTypeword;
            break;
        }
    }
    return strTemp;
}
