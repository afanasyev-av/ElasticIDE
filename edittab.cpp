#include "edittab.h"
#include "ui_edittab.h"

EditTab::EditTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditTab)
{
    ui->setupUi(this);
    highlighter = new Highlighter(ui->textEdit->document());

    connect(ui->textEdit->document(), SIGNAL(contentsChanged()),this, SLOT(documentWasModified()));
    //connect(this, SIGNAL(ui->textEdit->textChanged()),this,SLOT(wasChanged()));
    connect(ui->textEdit->mActOpenFile, SIGNAL(triggered()), parent, SLOT(openFileatCursor()));

    ui->textEdit_2->hide();
    highlighter = new Highlighter(ui->textEdit_2->document());
}

EditTab::~EditTab()
{
    delete ui;
}

void EditTab::documentWasModified() //Ставит метку модификации главного окна
{
    setWindowModified(ui->textEdit->document()->isModified());
}

void EditTab::setText(QString str)
{
    ui->textEdit->clear(); // unless you know the editor is empty
    ui->textEdit->appendPlainText(str);
}

void EditTab::setCompareText(QString str)
{
    ui->textEdit_2->clear(); // unless you know the editor is empty
    ui->textEdit_2->appendPlainText(str);
}

QString EditTab::getText(void)
{
    return ui->textEdit->toPlainText();
}

QTextDocument* EditTab::getDocument(void)
{
    return ui->textEdit->document();
}

QPlainTextEdit* EditTab::getPlainText(void)
{
    return ui->textEdit;
}

void EditTab::ShowCompare(bool checked)
{
    ui->textEdit_2->setVisible(checked);
}
