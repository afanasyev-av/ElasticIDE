#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QFontDialog>
#include "highlighter.h"

namespace Ui {
class OptionsDialog;
}

class OptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();

    QString compiler;
    QString executer;
private slots:
    void on_pushButton_pressed();

    void on_pushButton_2_clicked();

    void on_pushButton_2_pressed();

    void on_pushButton_3_clicked();

private:
    Ui::OptionsDialog *ui;
    Highlighter *highlighter;
};

#endif // OPTIONSDIALOG_H
