#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QSyntaxHighlighter>

#include <QHash>
#include <QTextCharFormat>
#include <QTextDocument>

//#include <QMessageBox>
//class QTextDocument;

class Highlighter : public QSyntaxHighlighter
{
    Q_OBJECT

    public:
        Highlighter(QTextDocument *parent = 0);

    protected:
        void highlightBlock(const QString &str) Q_DECL_OVERRIDE;

    private:
        QStringList m_lstKeywords;
        QStringList m_lstTypewords;
        QStringList m_lstDBwords;

        enum { NormalState = -1, InsideCStyleComment, InsideCString , InsidePHPString};
        QString getKeyword (int i, const QString str);
        QString getTypeword (int i, const QString str);
        QString getDBword (int i, const QString str);
        //struct HighlightingRule
        //{
        //    QRegExp pattern;
        //    QTextCharFormat format;
        //};
        //QVector<HighlightingRule> highlightingRules;

        //QRegExp commentStartExpression;
        //QRegExp commentEndExpression;
        //QRegExp CstringStartExpression;
        //QRegExp CstringEndExpression;
        //QRegExp PHPstringStartExpression;
        //QRegExp PHPstringEndExpression;
    public:
        QTextCharFormat keywordFormat;
        QTextCharFormat typeFormat;
        QTextCharFormat databaseFormat;
        QTextCharFormat CommentFormat;
        QTextCharFormat preprocessorFormat;
        QTextCharFormat cstringFormat;
};

#endif
