#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QProcess>
//#include <QLibrary>
#include <QTextCodec>
#include <QInputDialog>
#include <QSettings>
#include<QMessageBox>
#include <QTextCodec>

#include "edittab.h"
#include "optionsdialog.h"
//#include "codeeditor.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionRun_triggered();
    void showOutput();
    void on_actionStop_triggered();
    void on_actionNew_triggered();
    void closeTab(int index);
    void on_actionSaveAs_triggered();
    void on_actionSettings_triggered();
    void on_actionFind_triggered();
    void on_actionFind_next_triggered();
    void documentWasModified();
    void on_actionCompare_triggered();
    void on_actionSet_Codec_triggered();
    void openFileatCursor();

private:
    void openFile(QString FileName);

private:
    Ui::MainWindow *ui;
    QProcess myProc;
    QString compiler;
    QString executer;
    QTextCursor cursor;
    QString searchString;
    //QSettings setting;
    QTextCodec* textcodec;

};

#endif // MAINWINDOW_H
