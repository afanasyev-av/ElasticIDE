#ifndef EDITTAB_H
#define EDITTAB_H

#include <QWidget>
#include <QPlainTextEdit>
#include "highlighter.h"


namespace Ui {
class EditTab;
}

class EditTab : public QWidget
{
    Q_OBJECT

public:
    explicit EditTab(QWidget *parent = 0);
    ~EditTab();

    void setText(QString str);
    void setCompareText(QString str);
    QString getText(void);
    QTextDocument* getDocument(void);
    QPlainTextEdit* getPlainText(void);
    void ShowCompare(bool checked);

    QString FileName;
private:
    Ui::EditTab *ui;
    Highlighter *highlighter;
    Highlighter *highlighter2;

private slots:
    void documentWasModified(void);
//    void wasChanged();

};

#endif // EDITTAB_H
